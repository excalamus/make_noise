# Make Noise

![the make noise application](extras/overview.png "Make Noise")

## What is **make_noise**?
**make_noise** is a Windows and GNU/Linux desktop application to generate white noise.

It is useful for masking other noises, such as those common in open office spaces.

![What it sounds like](extras/output.wav "What it sounds like")

## Quick start
[Binaries exist](https://codeberg.org/excalamus/make_noise/releases) for both Windows and Linux.  You may also try running or
building [from source](#From-source).

Start and stop are toggled using a taskbar button.  

No noise is generated when the button is green.  Double click the green button to make noise:

![a green button](extras/green-button.png "Green Button")  <-- No noise

Noise is generated when the button is red.  Double clicking the red button stops the noise:

![a red button](extras/red-button.png "Red Button")  <-- Noise

## Design
**make_noise** is a minimal graphical interface for [the `noise.sh` gist](https://gist.github.com/xguse/6259275).  It provides a taskbar button to toggle noise on and off.  It works by constructing a sox call string, starting a subprocess, and passing the sox call string.

The string call used is:

```
sox --no-show-progress -c 2 --null synth 3600 brownnoise band -n 1500 499 tremolo 0.05 43 reverb 19 bass -11  treble -1 vol 14dB  fade q .01 repeat 9999
```

Noise is stopped by killing the subprocess.

## From source
This software is licensed under the GPL.

Requirements known to work:

- Python 3.7.3 (could probably use 3.6)
- PySide2 (built on Linux with 5.15.1, Windows 5.15.2)
- SoX v14.4.2

Binaries were created with PyInstaller 4.2 using the command:

```
pyinstaller make_noise.spec --onefile
```
